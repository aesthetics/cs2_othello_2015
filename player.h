#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <cmath>
using namespace std;

class Player {
	Side ourSide;
	Side opponentSide;
	Board *newBoard;
public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};
	
int score(int i, int j, Board *Board, Side side);
int minimax(Board *theBoard, Side ourSide, int depth);
int score2(Board *board, Side side);

#endif
