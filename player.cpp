#include <stdio.h>
#include "player.h"
#include <vector>
/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */

int MAX_DEPTH = 5;

Player::Player(Side side) {
    // Will be set to TRUE in test_minimax.cpp.
    testingMinimax = false;
	if (side == BLACK)
	{
		ourSide = BLACK;
		opponentSide = WHITE;
	}
	else 
	{
		ourSide = WHITE;
		opponentSide = BLACK;
	}
    newBoard = new Board();
   
}
/*
 * Destructor for the player.
 */
Player::~Player() {
}

/**
Move *Player::doMove(Move *opponentsMove, int msLeft) 
{
	newBoard->doMove(opponentsMove, opponentSide);
	if (not newBoard->hasMoves(ourSide))
		return NULL;
	int i = 0;
	int j = 0;
	Move *move = new Move(0, 0);
    while (not newBoard->checkMove(move, ourSide))
    {
		if (i != 8)
		{
			i++;
		}
		else
		{
			i = 0;
			j++;
		}
		move->x = i;
		move->y = j;
    }

    newBoard->doMove(move, ourSide);
    return (move);
}
**/
// HEURISTIC //
/**
Move *Player::doMove(Move *opponentsMove, int msLeft) 
{
	newBoard->doMove(opponentsMove, opponentSide);
	if (not newBoard->hasMoves(ourSide))
	{
		return NULL;
	}
    int moveX = 0;
    int moveY = 0;
    int theScore = -1000000;
    for (int i = 0; i < 8; i++)
    {
		 for (int j = 0; j < 8; j++)
		 {
			 Move *potentialMove = new Move(i,j);
			 if (newBoard->checkMove(potentialMove, ourSide))
			 {
				if (theScore <= score(i, j, newBoard, ourSide));
				{
					theScore = score(i, j, newBoard, ourSide); 
					moveX = i;
					moveY = j;
				}
			}
		 }
	}
	Move *move = new Move(moveX, moveY);
	newBoard->doMove(move, ourSide);
	return move;
}
**/

Move *Player::doMove(Move *opponentsMove, int msLeft) 
{
	newBoard->doMove(opponentsMove, opponentSide);
	if (not newBoard->hasMoves(ourSide))
	{
		return NULL;
	}
	int moveX = 0;
    int moveY = 0;
	int theScore = -1000000;
	for (int i = 0; i < 8; i++)
    {
		 for (int j = 0; j < 8; j++)
		 {
			 Move *potentialMove = new Move(i,j);
			 if (newBoard->checkMove(potentialMove, ourSide))
			 {
				Board *tempboard = newBoard->copy();
				tempboard->doMove(potentialMove, ourSide);
				int min = minimax(tempboard, ourSide, 0);
				if (theScore <= min);
				{
					theScore = min; 
					moveX = i;
					moveY = j;
				}
			 }
			 delete potentialMove;
		 }
	}
	Move *move = new Move(moveX, moveY);
	newBoard->doMove(move, ourSide);
	return move;
}
/**
int minimax(Board *theBoard, Move *move, Side ourSide)
{
	int mini = 10000;
	std::vector<Board*> boardList;
	Board *tboard = theBoard->copy();
	tboard->doMove(move, ourSide);
	boardList.push_back(tboard);
	int depth = 0;
	while (depth < MAX_DEPTH)
	{
		std::vector<Board*> nextDepth;
		int size = boardList.size();
		for (int j = 0; j < size; j++)
		{
			for (int x = 0; x < 8; x++)
			{
				for (int y = 0; y < 8; y++)
				{
					Move *potentialMove = new Move(x,y);
					if (boardList[j]->checkMove(potentialMove, ourSide))
					{
						Board *cboard = boardList[j]->copy();
						cboard->doMove(potentialMove, ourSide);
						nextDepth.push_back(cboard);
					}		
					delete potentialMove;
				}	
			}
		}
		boardList.clear();
		size = nextDepth.size();
		for (int e = 0; e < size; e++)
		{
			boardList.push_back(nextDepth[e]);
		}
		nextDepth.clear();
		depth++;
	}
	
	int size = boardList.size();
	for (int m = 0; m < size; m++)
	{
		if (score2(boardList[m], ourSide) < mini)
		{
			mini = score2(boardList[m], ourSide);
		}
	}
	return mini;
}
**/
int score2(Board *cboard, Side side)
{
	int score;
	if(side == BLACK)
    {
      score = cboard->countBlack() - cboard->countWhite();
    }
	else 
    {
      score = cboard->countWhite() - cboard->countBlack();
    }
	return score;
}

/**
int score(int coordx, int coordy, Board *board, Side side)
{
  Board *cboard = board->copy();
  Move *move = new Move(coordx, coordy);
  int score = 1;
  cboard->doMove(move, side);
  
  for (int i = 0; i < 8; i++)
    {
      for (int j = 0; j < 8; j++)
	{
	  if(cboard->get(BLACK, i, j) == 1)
	    {
	      score += 1;
	    }
	  else if(cboard->get(WHITE, i, j) == 1)
	    {
	      score += -1;
	    }
	}
    }


  if(cboard->get(BLACK, 0, 0) == 1)
    {
      score += 100;
    }
  else if(cboard->get(WHITE, 0, 0) == 1)
    {
      score += -100;
    }
  
  if(cboard->get(BLACK, 0, 7) == 1)
    {
      score += 100;
    }
  else if(cboard->get(WHITE, 0, 7) == 1)
    {
      score += -100;
    }
  
  if(cboard->get(BLACK, 7, 0) == 1)
    {
      score += 100;
    }
  else if(cboard->get(WHITE, 7, 0) == 1)
    {
      score += -100;
    }
  
  if(cboard->get(BLACK, 7, 7) == 1)
    {
      score += 100;
    }
  else if(cboard->get(WHITE, 7, 7) == 1)
    {
      score += -100;
    }
  
  // For places giving access to corners
  
  if(cboard->get(BLACK, 1, 1) == 1)
    {
      score += -100;
    }
  else if(cboard->get(WHITE, 1, 1) == 1)
    {
      score += 100;
    }
  
  
  if(cboard->get(BLACK, 1, 0) == 1)
    {
      score += -100;
    }
  else if(cboard->get(WHITE, 1, 0) == 1)
    {
      score += 100;
    }
  
  if(cboard->get(BLACK, 0, 1) == 1)
    {
      score += -100;
    }
  else if(cboard->get(WHITE, 0, 1) == 1)
    {
      score += 100;
    }
  
  if(cboard->get(BLACK, 1, 6) == 1)
    {
      score += -100;
    }
  else if(cboard->get(WHITE, 1, 6) == 1)
    {
      score += 100;
    }
  
  if(cboard->get(BLACK, 1, 7) == 1)
    {
      score += -100;
    }
  else if(cboard->get(WHITE, 1, 7) == 1)
    {
      score += 100;
    }
  
  if(cboard->get(BLACK, 0, 6) == 1)
    {
      score += -100;
    }
  else if(cboard->get(WHITE, 0, 6) == 1)
    {
      score += 100;
    }
  
  if(cboard->get(BLACK, 6, 1) == 1)
    {
      score += -100;
    }
  else if(cboard->get(WHITE, 6, 1) == 1)
    {
      score += 100;
    }
  
  if(cboard->get(BLACK, 6, 0) == 1)
    {
      score += -100;
    }
  else if(cboard->get(WHITE, 6, 0) == 1)
    {
      score += 100;
    }
  
  if(cboard->get(BLACK, 7, 1) == 1)
    {
      score += -100;
    }
  else if(cboard->get(WHITE, 7, 1) == 1)
    {
      score += 100;
    }
  
  if(cboard->get(BLACK, 6, 6) == 1)
    {
      score += -100;
    }
  else if(cboard->get(WHITE, 6, 6) == 1)
    {
      score += 100;
    }
  
  if(cboard->get(BLACK, 6, 7) == 1)
    {
      score += -100;
    }
  else if(cboard->get(WHITE, 6, 7) == 1)
    {
      score += 100;
    }
  
  if(cboard->get(BLACK, 7, 6) == 1)
    {
      score += -100;
    }
  else if(cboard->get(WHITE, 7, 6) == 1)
    {
      score += 100;
    }
  
  for(int i = 2; i < 6; i++)
    {
      if(cboard->get(BLACK, i, 0) == 1)
	{
	  score += 5;
	}
      else if(cboard->get(WHITE, i, 0) == 1)
	{
	  score += -5;
	}
    }
  
  
  for(int i = 2; i < 6; i++)
    {
      if(cboard->get(BLACK, 0, i) == 1)
	{
	  score += 5;
	}
      else if(cboard->get(WHITE, 0, i) == 1)
	{
	  score += -5;
	}
    }

  
  for(int i = 2; i < 6; i++)
    {
      if(cboard->get(BLACK, i, 7) == 1)
	{
	  score += 5;
	}
      else if(cboard->get(WHITE, i, 7) == 1)
	{
	  score += -5;
	}
    }
  
  
  for(int i = 2; i < 6; i++)
    {
      if(cboard->get(BLACK, 7, i) == 1)
	{
	  score += 5;
	}
      else if(cboard->get(WHITE, 7, i) == 1)
	{
	  score += -5;
	}
    }
  

  if(side == WHITE)
    {
      score *= -1;
    }
  
  return score;
}
**/

int minimax(Board *theBoard, Side ourSide, int depth)
{
	int minny = 10000;
	
	if (depth > MAX_DEPTH)
	{
		int theScore = score2(theBoard, ourSide);
		return theScore;
	}
	
	for (int x = 0; x < 8; x++)
	{
		for (int y = 0; y < 8; y++)
		{
			Move *potentialMove = new Move(x,y);
			if (theBoard->checkMove(potentialMove, ourSide))
			{
				Board *cboard = theBoard->copy();
				cboard->doMove(potentialMove, ourSide);
				int min = minimax(cboard, ourSide, depth + 1);
				delete cboard;
				if (min < minny)
				{
					minny = min;
				}
			}	
			delete potentialMove;
		}
	}
}


