Both of us went over general strategy and algorithm implementation for both weeks,
determining what our minimax function would entail and the specifics of our heuristic.
Henry was in charge of implementing minimax while Dave focused on creating as
optimal of a heuristic function as possible. While we mostly stuck to our assigned roles,
when we ran into trouble the other would help out. 

This past week, we've implemented a minimax function which can search to depth 5,
along with a new-and-improved heuristic score function, which should be sure to help
us get some pivotal wins this Tuesday. The main idea that didn't pan out was avoiding
recursion in minimax by holding Boards in a vector and searching through each Board
in the vector then adding the deeper, newer Boards to the vector for faster search.
Unfortunately, we ran into several problems with memory and had to abandon this
approach in favor of a recursive method, which did the job well. 